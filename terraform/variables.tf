#############################################################################
# VARIABLES
#############################################################################
# JMS = Jenkins Master/Slave

#########################################################################
################## AWS VPC configuration variables ######################
#########################################################################
variable "region_east" {
  type = string
  default = "us-east-1"
}

variable "azs" {
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "vpc_name" {
  type = string
  default = "jenkins-ms-vpc-prod"
  description = "Jenkins Master/Slave VPC for Production"
}

variable "vpc_cidr_block" {
  type = string
  default = "10.0.0.0/16"
}

variable "subnets" {
  type = map(string)
  default = {
    "backend" = "Jenkins Backend Subnet"
    "bastion" = "Jenkins Bastion Subnet"
    "public"  = "Jenkins Public Subnet"
  }
}

variable "public_subnet" {
  type = list(string)
  default = [ "10.0.20.0/24", "10.0.21.0/24"]
}

variable "private_subnet" {
    type = list(string)
    default = [ "10.0.30.0/24", "10.0.31.0/24" ]
  
}

variable "bastion_subnet" {
    type = map(string)
    default = {
      "bastion" = "10.0.0.0/24"
    } 
}

variable "route_table" {
  type = map(string)
  default = {
    "public" = "Public Route Table"
    "private" = "Private Route Table"
  }
}

variable "internet_gateways" {
  type = string
  default = "Internet Gateway"
}

variable "nat_gateways" {
  type = string
  description = "Nat Gateway"
}

variable "network_acl" {
  type = map(string)
  default = {
    "vpc" = "VPC ACL"
    "bastion" = "Bastion ACL"
    "public" = "Public ACL"
    "private" = "Private ACL"
  }
}

variable "security_groups" {
  type = map(string)
  default = {
    "backend-alb" = "Backend Application Load Balancer Security Group"
    "backend" = "Backend Security Group"
    "bastion" = "Bastion Security Group"
    "public"  = "Public Security Group"
  }
}

variable "security_group_lbl" {
  default = "Load Balancer Access Group"
}

variable "allIPsCIDRblock" {
  default = "0.0.0.0/0"
}

variable "mapPublicIP" {
  default = true
}

#########################################################################
############### AWS EC2 Instance configuration variables ################
#########################################################################

variable "key_name" {
  default = "JMS-KP"
  description = "AWS Key Pair for SSH Instance Access"
}

variable "key_pair" {
  type = map(string)
  default = {
    "key_name" = var.key_name    # TO-DO CHECK
    "pub_key" = var.key_name     # TO-DO CHECK
  }
}

variable "instance_types" {
  type = map(string)
  default = {
    "dev" = "t2.micro"
    "qa" = "t2.micro"
    "prod" = "t2.medium"
  }
}

variable "ssh_user" {
  type = string
  default = "ec2-user"
}

